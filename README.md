# bee-jmocker

蜜蜂生产蜂蜜，bee-jmocker生产数据。

## 用法

参考测试用例：

```java
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BeeMockerDataTestApplication.class)
public class BeeMockerDataTestApplication {

    @Test
    public void testBean(){
        TestBean testBean = BeeMockerData.mock(TestBean.class);
        assertNotNull(testBean);
    }

    @Test
    public void testArray(){
        Byte[] byteArray = BeeMockerData.mock(Byte[].class);
        assertNotNull(byteArray);
    }

    @Test
    public void testRange(){
        MockConfig config = new MockConfig();
        config.intRange(0, 20)
        .sizeRange(15, 15);
        int a = BeeMockerData.mock(int.class, config);
        Assert.assertTrue(a > 0 && a < 20);

        int[] array = BeeMockerData.mock(int[].class, config);
        Assert.assertEquals(15, array.length);
    }

    @Test
    public void testPrime(){
        int num1 = BeeMockerData.mockPrime(int.class, 0, 9);
        Assert.assertTrue(num1<=9 && num1>=0);

        Integer num2 = BeeMockerData.mockPrime(Integer.class, 9, 18);
        Assert.assertTrue(num2<=18 && num2>=9);

        long num3 = BeeMockerData.mockPrime(long.class, 2l, 4l);
        Assert.assertTrue(num3>=2l && num3<=4l);
    }

    @Test
    public void testBeanFieldRange(){
        TestBean bean = BeeMockerData.mock(TestBean.class);
        Assert.assertNotNull(bean);
    }

}
```