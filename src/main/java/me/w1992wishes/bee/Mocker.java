package me.w1992wishes.bee;

/**
 * @Description 模拟器接口
 *
 * @Author w1992wishes
 * @Date 2018/6/27 19:34
 * @Version 1.0
 */
public interface Mocker<T> {

    /**
     * 模拟数据
     *
     * @param mockConfig 模拟数据配置
     * @return 模拟数据对象
     */
    T mock(MockConfig mockConfig);

}
