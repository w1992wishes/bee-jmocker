package me.w1992wishes.bee.mocker;

import me.w1992wishes.bee.MockConfig;
import me.w1992wishes.bee.Mocker;
import me.w1992wishes.bee.util.RandomUtils;

/**
 * @Description Boolean对象模拟器
 *
 * @Author w1992wishes
 * @Date 2018/6/27 19:42
 * @Version 1.0
 */
public class BooleanMocker implements Mocker<Boolean> {

  @Override
  public Boolean mock(MockConfig mockConfig) {
    return RandomUtils.nextBoolean();
  }

}