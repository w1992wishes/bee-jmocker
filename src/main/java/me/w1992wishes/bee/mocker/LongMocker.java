package me.w1992wishes.bee.mocker;

import me.w1992wishes.bee.MockConfig;
import me.w1992wishes.bee.Mocker;
import me.w1992wishes.bee.util.RandomUtils;

/**
 * @Description Long对象模拟器
 *
 * @Author w1992wishes
 * @Date 2018/6/27 19:44
 * @Version 1.0
 */

public class LongMocker implements Mocker<Long> {

    @Override
    public Long mock(MockConfig mockConfig) {
        return RandomUtils.nextLong(mockConfig.getLongRange()[0], mockConfig.getLongRange()[1]);
    }

}
