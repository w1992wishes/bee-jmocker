package me.w1992wishes.bee.mocker;

import me.w1992wishes.bee.MockConfig;
import me.w1992wishes.bee.Mocker;

/**
 * @Description ClassMocker
 *
 * @Author w1992wishes
 * @Date 2018/6/27 20:19
 * @Version 1.0
 */
public class ClassMocker implements Mocker<Object> {

    private Class clazz;

    ClassMocker(Class clazz) {
        this.clazz = clazz;
    }

    @Override
    public Object mock(MockConfig mockConfig) {
        Mocker mocker;
        if (clazz.isArray()){
            mocker = new ArrayMocker(clazz);
        } else {
            mocker = mockConfig.getMocker(clazz);
            if (mocker == null) {
                mocker = new BeanMocker(clazz);
            }
        }
        return mocker.mock(mockConfig);
    }
}
