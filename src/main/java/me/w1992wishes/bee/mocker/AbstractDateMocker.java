package me.w1992wishes.bee.mocker;

import me.w1992wishes.bee.exception.BeeMockerException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * AbstractDateMocker
 */
public class AbstractDateMocker {
  private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

  protected Long startTime;
  protected Long endTime;

  public AbstractDateMocker(String startTimePattern, String endTimePattern) {
    try {
      this.startTime = FORMAT.parse(startTimePattern).getTime();
      this.endTime = FORMAT.parse(endTimePattern).getTime();
    } catch (ParseException e) {
      throw new BeeMockerException("时间格式必须为 yyyy-MM-dd HH:mm:ss ", e);
    }
  }
}