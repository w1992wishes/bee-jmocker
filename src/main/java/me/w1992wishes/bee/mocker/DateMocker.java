package me.w1992wishes.bee.mocker;

import me.w1992wishes.bee.MockConfig;
import me.w1992wishes.bee.Mocker;
import me.w1992wishes.bee.util.RandomUtils;

import java.util.Date;

/**
 * @Description Date对象模拟器
 *
 * @Author w1992wishes
 * @Date 2018/6/27 19:52
 * @Version 1.0
 */
public class DateMocker extends AbstractDateMocker implements Mocker<Date> {

    public DateMocker(String startTimePattern, String endTime) {
        super(startTimePattern, endTime);
    }

    @Override
    public Date mock(MockConfig mockConfig) {
        return new Date(RandomUtils.nextLong(startTime, endTime));
    }

}
