package me.w1992wishes.bee.mocker;

import me.w1992wishes.bee.MockConfig;
import me.w1992wishes.bee.Mocker;
import me.w1992wishes.bee.util.RandomUtils;

/**
 * @Description Double对象模拟器
 *
 * @Author w1992wishes
 * @Date 2018/6/27 19:37
 * @Version 1.0
 */
public class DoubleMocker implements Mocker<Double> {

  @Override
  public Double mock(MockConfig mockConfig) {
    return RandomUtils.nextDouble(mockConfig.getDoubleRange()[0], mockConfig.getDoubleRange()[1]);
  }

}