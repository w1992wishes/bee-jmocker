package me.w1992wishes.bee.mocker;

import me.w1992wishes.bee.MockConfig;
import me.w1992wishes.bee.Mocker;
import me.w1992wishes.bee.annotation.*;
import me.w1992wishes.bee.exception.BeeMockerException;
import me.w1992wishes.bee.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * @Description bean对象模拟器
 *
 * @Author w1992wishes
 * @Date 2018/6/27 20:00
 * @Version 1.0
 */
public class BeanMocker implements Mocker<Object> {

    private final Class clazz;

    BeanMocker(Class clazz) {
        this.clazz = clazz;
    }

    @Override
    public Object mock(MockConfig mockConfig) {
        try {
            if (mockConfig.isEnabledCircle()) {
                Object cacheBean = mockConfig.getcacheBean(clazz.getName());
                if (cacheBean != null) {
                    return cacheBean;
                }
            }
            Object result = clazz.newInstance();
            mockConfig.cacheBean(clazz.getName(), result);
            for (Class<?> currentClass = clazz; currentClass != Object.class; currentClass = currentClass.getSuperclass()) {
                // 模拟有setter方法的字段
                for (Map.Entry<Field, Method> entry : ReflectionUtils.fieldAndSetterMethod(currentClass).entrySet()) {
                    Field field = entry.getKey();
                    if (field.isAnnotationPresent(MockIgnore.class)) {
                        continue;
                    }
                    // 根据每个实体域上的注解配置值范围
                    if (field.getDeclaredAnnotations().length != 0){
                        mockConfig = config(field, mockConfig);
                    } else {
                        mockConfig.reset();
                    }
                    ReflectionUtils.setRefValue(result, entry.getValue(), new BaseMocker(field.getGenericType()).mock(mockConfig));
                }
            }
            return result;
        } catch (Exception e) {
            throw new BeeMockerException(e);
        }
    }

    private MockConfig config(Field field, MockConfig mockConfig){
        if (field.isAnnotationPresent(IntRange.class)
                && (field.getType().getSimpleName().equalsIgnoreCase("int")
                || field.getType().getSimpleName().equalsIgnoreCase("Integer"))){
            mockConfig.intRange(field.getAnnotation(IntRange.class).min(), field.getAnnotation(IntRange.class).max());
        } else if (field.isAnnotationPresent(DoubleRange.class)
                && field.getType().getSimpleName().equalsIgnoreCase("double")) {
            mockConfig.doubleRange(field.getAnnotation(DoubleRange.class).min(), field.getAnnotation(DoubleRange.class).max());
        } else if (field.isAnnotationPresent(LongRange.class)
                && field.getType().getSimpleName().equalsIgnoreCase("long")) {
            mockConfig.longRange(field.getAnnotation(LongRange.class).min(), field.getAnnotation(LongRange.class).max());
        } else if (field.isAnnotationPresent(ShortRange.class)
                && field.getType().getSimpleName().equalsIgnoreCase("short")) {
            mockConfig.shortRange(field.getAnnotation(ShortRange.class).min(), field.getAnnotation(ShortRange.class).max());
        } else if (field.isAnnotationPresent(ByteRange.class)
                && field.getType().getSimpleName().equalsIgnoreCase("byte")) {
            mockConfig.byteRange(field.getAnnotation(ByteRange.class).min(), field.getAnnotation(ByteRange.class).max());
        } else if (field.isAnnotationPresent(FloatRange.class)
                && field.getType().getSimpleName().equalsIgnoreCase("float")) {
            mockConfig.floatRange(field.getAnnotation(FloatRange.class).min(), field.getAnnotation(FloatRange.class).max());
        } else if (field.isAnnotationPresent(DateRange.class)
                && field.getType().getSimpleName().equalsIgnoreCase("date")) {
            mockConfig.dateRange(field.getAnnotation(DateRange.class).min(), field.getAnnotation(DateRange.class).max());
        } else if (field.isAnnotationPresent(SizeRange.class)
                && (field.getType().getSimpleName().equalsIgnoreCase("byte[]")
                || field.getType().getSimpleName().equalsIgnoreCase("short[]")
                || field.getType().getSimpleName().equalsIgnoreCase("float[]")
                || field.getType().getSimpleName().equalsIgnoreCase("double[]")
                || field.getType().getSimpleName().equalsIgnoreCase("int[]")
                || field.getType().getSimpleName().equalsIgnoreCase("Integer[]")
                || field.getType().getSimpleName().equalsIgnoreCase("long[]")
                || field.getType().getSimpleName().equalsIgnoreCase("char[]")
                || field.getType().getSimpleName().equalsIgnoreCase("string"))) {
            mockConfig.sizeRange(field.getAnnotation(SizeRange.class).min(), field.getAnnotation(SizeRange.class).max());
        } else {
            mockConfig.reset();
        }
        return mockConfig;
    }
}
