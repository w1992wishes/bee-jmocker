package me.w1992wishes.bee.mocker;

import me.w1992wishes.bee.MockConfig;
import me.w1992wishes.bee.Mocker;
import me.w1992wishes.bee.util.RandomUtils;

/**
 * @Description Byte对象模拟器
 *
 * @Author w1992wishes
 * @Date 2018/6/27 19:54
 * @Version 1.0
 */
public class ByteMocker implements Mocker<Byte> {

  @Override
  public Byte mock(MockConfig mockConfig) {
    return (byte) RandomUtils.nextInt(mockConfig.getByteRange()[0], mockConfig.getByteRange()[1]);
  }

}
