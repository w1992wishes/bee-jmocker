package me.w1992wishes.bee.mocker;

import me.w1992wishes.bee.MockConfig;
import me.w1992wishes.bee.Mocker;
import me.w1992wishes.bee.util.RandomUtils;

import java.lang.reflect.*;

/**
 * @Description Array对象模拟器
 *
 * @Author w1992wishes
 * @Date 2018/6/28 9:44
 * @Version 1.0
 */
public class ArrayMocker implements Mocker<Object> {

    private Type type;

    ArrayMocker(Type type) {
        this.type = type;
    }

    @Override
    public Object mock(MockConfig mockConfig) {
        return array(mockConfig);
    }

    private Object array(MockConfig mockConfig) {
        int size = RandomUtils.nextSize(mockConfig.getSizeRange()[0], mockConfig.getSizeRange()[1]);
        Class componentClass = ((Class) type).getComponentType();
        Object result = Array.newInstance(componentClass, size);
        BaseMocker baseMocker = new BaseMocker(componentClass);
        for (int index = 0; index < size; index++) {
            Array.set(result, index, baseMocker.mock(mockConfig));
        }
        return result;
    }

}
