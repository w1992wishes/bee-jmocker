package me.w1992wishes.bee.mocker;

import me.w1992wishes.bee.MockConfig;
import me.w1992wishes.bee.Mocker;
import me.w1992wishes.bee.util.RandomUtils;

/**
 * @Description Character对象模拟器
 *
 * @Author w1992wishes
 * @Date 2018/6/27 20:13
 * @Version 1.0
 */
public class CharacterMocker implements Mocker<Character> {

  @Override
  public Character mock(MockConfig mockConfig) {
    char[] charSeed = mockConfig.getCharSeed();
    return charSeed[RandomUtils.nextInt(0, charSeed.length)];
  }

}