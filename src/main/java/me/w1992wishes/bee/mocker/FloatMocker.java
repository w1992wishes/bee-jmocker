package me.w1992wishes.bee.mocker;

import me.w1992wishes.bee.MockConfig;
import me.w1992wishes.bee.Mocker;
import me.w1992wishes.bee.util.RandomUtils;

/**
 * @Description Float对象模拟器
 *
 * @Author w1992wishes
 * @Date 2018/6/27 19:37
 * @Version 1.0
 */
public class FloatMocker implements Mocker<Float> {

  @Override
  public Float mock(MockConfig mockConfig) {
    return RandomUtils.nextFloat(mockConfig.getFloatRange()[0], mockConfig.getFloatRange()[1]);
  }

}