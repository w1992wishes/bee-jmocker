package me.w1992wishes.bee.mocker;

import me.w1992wishes.bee.MockConfig;
import me.w1992wishes.bee.Mocker;
import me.w1992wishes.bee.util.RandomUtils;

/**
 * @Description Integer对象模拟器
 *
 * @Author w1992wishes
 * @Date 2018/6/27 19:37
 * @Version 1.0
 */
public class IntegerMocker implements Mocker<Integer> {

    @Override
    public Integer mock(MockConfig mockConfig) {
        return RandomUtils.nextInt(mockConfig.getIntRange()[0], mockConfig.getIntRange()[1]);
    }

}
