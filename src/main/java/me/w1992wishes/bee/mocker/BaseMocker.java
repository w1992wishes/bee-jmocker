package me.w1992wishes.bee.mocker;

import me.w1992wishes.bee.MockConfig;
import me.w1992wishes.bee.Mocker;

import java.lang.reflect.Type;

/**
 * @Description
 *
 * @Author w1992wishes
 * @Date 2018/6/27 20:13
 * @Version 1.0
 */
public class BaseMocker<T> implements Mocker<T> {

    private Type type;

    public BaseMocker(Type type) {
        this.type = type;
    }

    @Override
    public T mock(MockConfig mockConfig) {
        return (T) new ClassMocker((Class) type).mock(mockConfig);
    }

}
