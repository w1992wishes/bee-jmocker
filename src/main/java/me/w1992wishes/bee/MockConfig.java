package me.w1992wishes.bee;

import me.w1992wishes.bee.mocker.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description 模拟数据配置类
 *
 * @Author w1992wishes
 * @Date 2018/6/27 19:35
 * @Version 1.0
 */
public class MockConfig {

    private static final ByteMocker BYTE_MOCKER = new ByteMocker();
    private static final BooleanMocker BOOLEAN_MOCKER = new BooleanMocker();
    private static final ShortMocker SHORT_MOCKER = new ShortMocker();
    private static final IntegerMocker INTEGER_MOCKER = new IntegerMocker();
    private static final LongMocker LONG_MOCKER = new LongMocker();
    private static final FloatMocker FLOAT_MOCKER = new FloatMocker();
    private static final DoubleMocker DOUBLE_MOCKER = new DoubleMocker();
    private static final CharacterMocker CHARACTER_MOCKER = new CharacterMocker();
    private static final StringMocker STRING_MOCKER = new StringMocker();
    private static final DateMocker DATE_MOCKER = new DateMocker("1970-01-01 00:00", "2100-12-31 00:00");

    /**
     * Bean缓存
     */
    private Map<String, Object> beanCache = new HashMap<>();

    private Map<Class<?>, Mocker> mockerContext = new HashMap<>();

    /**
     * 值 range
     */
    private static final byte DEFAULT_BYTE_MIN = 0;
    private static final byte DEFAULT_BYTE_MAX = 127;
    private static final short DEFAULT_SHORT_MIN = 0;
    private static final short DEFAULT_SHORT_MAX = 10000;
    private static final int DEFAULT_INT_MIN = 0;
    private static final int DEFAULT_INT_MAX = 10000;
    private static final float DEFAULT_FLOAT_MIN = 0.0f;
    private static final float DEFAULT_FLOAT_MAX = 10000.00f;
    private static final double DEFAULT_DOUBLE_MIN = 0;
    private static final double DEFAULT_DOUBLE_MAX = 10000;
    private static final long DEFAULT_LONG_MIN = 0L;
    private static final long DEFAULT_LONG_MAX = 10000L;
    private static final String DEFAULT_DATE_MIN = "1970-01-01 00:00";
    private static final String DEFAULT_DATE_MAX = "2100-12-31 23:59:59";
    private static final int DEFAULT_SIZE_MIN = 1;
    private static final int DEFAULT_SIZE_MAX = 10;

    private byte[] byteRange = {DEFAULT_BYTE_MIN, DEFAULT_BYTE_MAX};
    private short[] shortRange = {DEFAULT_SHORT_MIN, DEFAULT_SHORT_MAX};
    private int[] intRange = {DEFAULT_INT_MIN, DEFAULT_INT_MAX};
    private float[] floatRange = {DEFAULT_FLOAT_MIN, DEFAULT_FLOAT_MAX};
    private double[] doubleRange = {DEFAULT_DOUBLE_MIN, DEFAULT_DOUBLE_MAX};
    private long[] longRange = {DEFAULT_LONG_MIN, DEFAULT_LONG_MAX};
    private String[] dateRange = {DEFAULT_DATE_MIN, DEFAULT_DATE_MAX};
    private int[] sizeRange = {DEFAULT_SIZE_MIN, DEFAULT_SIZE_MAX};
    private boolean enabledCircle=false;

    private char[] charSeed =
            {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
                    'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F',
                    'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

    private String[] stringSeed =
            {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
                    "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F",
                    "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

    public MockConfig() {
        registerMocker(BYTE_MOCKER, byte.class, Byte.class);
        registerMocker(BOOLEAN_MOCKER, boolean.class, Boolean.class);
        registerMocker(SHORT_MOCKER, short.class, Short.class);
        registerMocker(INTEGER_MOCKER, Integer.class, int.class);
        registerMocker(LONG_MOCKER, long.class, Long.class);
        registerMocker(FLOAT_MOCKER, float.class, Float.class);
        registerMocker(DOUBLE_MOCKER, double.class, Double.class);
        registerMocker(CHARACTER_MOCKER, char.class, Character.class);
        registerMocker(STRING_MOCKER, String.class);
        registerMocker(DATE_MOCKER, Date.class);
    }

    public void reset(){
        byteRange(DEFAULT_BYTE_MIN, DEFAULT_BYTE_MAX);
        shortRange(DEFAULT_SHORT_MIN, DEFAULT_SHORT_MAX);
        intRange(DEFAULT_INT_MIN, DEFAULT_INT_MAX);
        floatRange(DEFAULT_FLOAT_MIN, DEFAULT_FLOAT_MAX);
        doubleRange(DEFAULT_DOUBLE_MIN, DEFAULT_DOUBLE_MAX);
        longRange(DEFAULT_LONG_MIN, DEFAULT_LONG_MAX);
        dateRange(DEFAULT_DATE_MIN, DEFAULT_DATE_MAX);
        sizeRange(DEFAULT_SIZE_MIN, DEFAULT_SIZE_MAX);
    }

    public MockConfig byteRange(byte min, byte max) {
        this.byteRange[0] = min;
        this.byteRange[1] = max;
        return this;
    }

    public MockConfig shortRange(short min, short max) {
        this.shortRange[0] = min;
        this.shortRange[1] = max;
        return this;
    }

    public MockConfig intRange(int min, int max) {
        this.intRange[0] = min;
        this.intRange[1] = max;
        return this;
    }

    public MockConfig floatRange(float min, float max) {
        this.floatRange[0] = min;
        this.floatRange[1] = max;
        return this;
    }

    public MockConfig doubleRange(double min, double max) {
        this.doubleRange[0] = min;
        this.doubleRange[1] = max;
        return this;
    }

    public MockConfig longRange(long min, long max) {
        this.longRange[0] = min;
        this.longRange[1] = max;
        return this;
    }

    public MockConfig dateRange(String min, String max) {
        this.dateRange[0] = min;
        this.dateRange[1] = max;
        registerMocker(new DateMocker(min, max), Date.class);
        return this;
    }

    public MockConfig sizeRange(int min, int max) {
        this.sizeRange[0] = min;
        this.sizeRange[1] = max;
        return this;
    }

    public MockConfig stringSeed(String... stringSeed) {
        this.stringSeed = stringSeed;
        return this;
    }

    public MockConfig charSeed(char... charSeed) {
        this.charSeed = charSeed;
        return this;
    }

    public byte[] getByteRange() {
        return byteRange;
    }

    public short[] getShortRange() {
        return shortRange;
    }

    public int[] getIntRange() {
        return intRange;
    }

    public long[] getLongRange() {
        return longRange;
    }

    public float[] getFloatRange() {
        return floatRange;
    }

    public double[] getDoubleRange() {
        return doubleRange;
    }

    public int[] getSizeRange() {
        return sizeRange;
    }

    public String[] getStringSeed() {
        return stringSeed;
    }

    public char[] getCharSeed() {
        return charSeed;
    }

    public boolean isEnabledCircle() {
        return enabledCircle;
    }

    public void cacheBean(String name, Object bean) {
        beanCache.put(name, bean);
    }

    public Object getcacheBean(String beanClassName) {
        return beanCache.get(beanClassName);
    }

    public void registerMocker(Mocker mocker, Class<?>... clazzs) {
        for (Class<?> clazz : clazzs) {
            mockerContext.put(clazz, mocker);
        }
    }

    public Mocker getMocker(Class<?> clazz) {
        return mockerContext.get(clazz);
    }
}
