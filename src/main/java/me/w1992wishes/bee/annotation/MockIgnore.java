package me.w1992wishes.bee.annotation;

import java.lang.annotation.*;

/**
 * 忽略模拟注解
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MockIgnore {

}