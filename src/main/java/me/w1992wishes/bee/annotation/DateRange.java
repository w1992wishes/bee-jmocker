package me.w1992wishes.bee.annotation;

import java.lang.annotation.*;

/**
 * @author w1992wishes 2018/6/29 10:41
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DateRange {

    String min();

    String max();

}
