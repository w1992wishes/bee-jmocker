package me.w1992wishes.bee.annotation;

import java.lang.annotation.*;

/**
 * @author w1992wishes 2018/6/28 19:24
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DoubleRange {

    double min();

    double max();

}
