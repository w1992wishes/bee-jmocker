package me.w1992wishes.bee;

import me.w1992wishes.bee.exception.BeeMockerException;
import me.w1992wishes.bee.mocker.BaseMocker;

/**
 * 模拟对象门面类
 *
 * @author w1992wishes 2018/6/27 19:35
 */
public class BeeMockerData {

    /**
     * 模拟数据
     *
     * @param clazz 模拟数据类型
     * @return 模拟数据对象
     */
    public static <T> T mock(Class<T> clazz) {
        return mock(clazz, new MockConfig());
    }

    /**
     * 模拟数据
     *
     * @param clazz 模拟数据类型
     * @param mockConfig 模拟数据配置
     * @param <T>
     * @return
     */
    public static <T> T mock(Class<T> clazz, MockConfig mockConfig) {
        return new BaseMocker<T>(clazz).mock(mockConfig);
    }

    /**
     * 模拟数据
     *
     * @param clazz 模拟数据类型
     * @param <T>
     * @return
     */
    public static <T> T mockPrime(Class<T> clazz) {
        if (!isPrimeOrWrap(clazz)){
            throw new BeeMockerException("the class must be prime type");
        }
        return new BaseMocker<T>(clazz).mock(new MockConfig());
    }

    /**
     * 模拟数据
     *
     * @param clazz 模拟数据类型
     * @param min
     * @param max
     * @param <T>
     * @return
     */
    public static <T> T mockPrime(Class<T> clazz, T min, T max) {
        if (!isPrimeOrWrap(clazz)){
            throw new BeeMockerException("the class must be prime type");
        }
        return new BaseMocker<T>(clazz).mock(config(clazz, min, max));
    }

    /**
     * 判断是否是基本类型及包装类
     *
     * @param clazz
     * @param <T>
     * @return
     */
    private static <T> boolean isPrimeOrWrap(Class<T> clazz){
        return clazz.getSimpleName().equalsIgnoreCase("int")
                || clazz.getSimpleName().equalsIgnoreCase("Integer")
                || clazz.getSimpleName().equalsIgnoreCase("double")
                || clazz.getSimpleName().equalsIgnoreCase("long")
                || clazz.getSimpleName().equalsIgnoreCase("short")
                || clazz.getSimpleName().equalsIgnoreCase("byte");
    }

    /**
     * 配置
     *
     * @param clazz
     * @param min
     * @param max
     * @param <T>
     * @return
     */
    private static <T> MockConfig config(Class<T> clazz, T min, T max){
        MockConfig mockConfig = new MockConfig();
        if (clazz.getSimpleName().equalsIgnoreCase("int") || clazz.getSimpleName().equalsIgnoreCase("Integer")){
            mockConfig.intRange((Integer) min, (Integer) max);
        } else if (clazz.getSimpleName().equalsIgnoreCase("double")){
            mockConfig.doubleRange((Double) min, (Double) max);
        } else if (clazz.getSimpleName().equalsIgnoreCase("long")){
            mockConfig.longRange((Long) min, (Long) max);
        } else if (clazz.getSimpleName().equalsIgnoreCase("short")){
            mockConfig.shortRange((Short) min, (Short) max);
        } else if (clazz.getSimpleName().equalsIgnoreCase("byte")){
            mockConfig.byteRange((Byte) min, (Byte) max);
        }
        return mockConfig;
    }
}
