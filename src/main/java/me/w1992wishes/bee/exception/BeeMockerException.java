package me.w1992wishes.bee.exception;

/**
 * @Description BeeMockerException
 *
 * @Author w1992wishes
 * @Date 2018/6/27 19:54
 * @Version 1.0
 */
public class BeeMockerException extends RuntimeException {

    public BeeMockerException(String message) {
        super(message);
    }

    public BeeMockerException(String message, Throwable cause) {
        super(message, cause);
    }

    public BeeMockerException(Throwable cause) {
        super(cause);
    }

}
