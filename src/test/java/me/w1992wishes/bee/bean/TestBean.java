package me.w1992wishes.bee.bean;

import lombok.Data;
import me.w1992wishes.bee.annotation.*;

import java.util.Date;

/**
 * @Description ifaas-analysis
 * @Author w1992wishes
 * @Date 2018/6/27 20:57
 * @Version 1.0
 */
@Data
public class TestBean {

    //基本类型
    @MockIgnore
    private byte byteNum;
    private boolean booleanNum;
    private char charNum;
    private short shortNum;
    private int integerNum;
    private long longNum;
    private float floatNum;
    private double doubleNum;

    //基本包装类型
    @MockIgnore
    private Byte byteBoxing;
    private Boolean booleanBoxing;
    private Character charBoxing;
    private Short shortBoxing;
    private Integer integerBoxing;
    private Long longBoxing;
    private Float floatBoxing;
    private Double doubleBoxing;

    //其他常用类型
    private Date date;
    private String string;

    // 注解范围
    @IntRange(min = 0, max = 3)
    private int num1;
    @IntRange(min = 0, max = 3)
    private Integer num2;

    @ByteRange(min = 4, max = 12)
    private byte num3;
    @ByteRange(min = 4, max = 12)
    private Byte num4;

    @ShortRange(min = 5, max = 11)
    private short num5;
    @ShortRange(min = 5, max = 11)
    private Short num6;

    @SizeRange(min = 5, max = 7)
    private byte[] nums;
}
